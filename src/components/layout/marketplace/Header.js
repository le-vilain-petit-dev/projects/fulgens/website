import React from 'react';
import AnchorLink from 'react-anchor-link-smooth-scroll';
import LogoIcon from '../../../svg/LogoIcon';
import Button from '../../Button';
import { navigate } from "gatsby"
import { Link } from 'gatsby'


const Header = () => (
  <header className="sticky top-0 bg-white shadow">
    <div className="container flex flex-col sm:flex-row justify-between items-center mx-auto py-4 px-8">
      <div className="flex items-center text-2xl">
        <div className="w-12 mr-3">
          <LogoIcon />
        </div>
        Fulgens Marketplace
      </div>
      <div className="flex mt-4 sm:mt-0">

      </div>
      <div className="md:block">
        <button className='py-3 px-8 bg-primary hover:bg-primary-darker rounded text-white' onClick={() => { navigate("/") }}>Go back Home</button>
      </div>
    </div>
  </header>
);

export default Header;
