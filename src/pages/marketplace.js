import React from 'react';
import PDFGenerator from '../components/PDFGenerator';
import Layout from '../components/layout/marketplace/Layout';
import HeroImage from '../svg/HeroImage';
import { useStaticQuery, graphql } from "gatsby"

const Index = () => {
  const data = useStaticQuery(graphql`{
    allDocx(filter: {name: {eq: "template"}}) {
        edges {
        node {
            content
            name
        }
        }
    }
    }
  `)

  return (
    <Layout>

      <section className="pt-10 md:pt-40">
        <div className="container mx-auto px-8 lg:flex">
          <div className="text-center lg:text-left lg:w-1/2">
            <h1 className="text-4xl lg:text-5xl xl:text-6xl font-bold leading-none">
              Prooth of concept
            </h1>
            <PDFGenerator data={data} />
          </div>
          <div className="lg:w-1/3">
            <HeroImage />
          </div>
        </div>
      </section>
    </Layout >
  )
};

export default Index;
